import sys
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

try:
    import argparse
except ImportError: 
    print >> sys.stderr, "[import error]", "Missing required module \"argparse\"" 
    sys.exit()

setup(name = 'kissplice2refgenome',
      version = '2.0.0',
#      description = '',
      author = 'Alice Julien-Laferriere, Camille Sessegolo, Camille Marchet, Clara Benoit-Pilven, Audric Cologne, Vincent Lacroix',
      author_email = 'audric.cologne@gmail.com',
      packages = ['lib'],
      scripts = ['kissplice2refgenome'],
      url = ['http://kissplice.prabi.fr/'],
      install_requires = ['argparse', 'sortedcontainers']
     )
