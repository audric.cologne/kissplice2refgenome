# KisSplice2RefGenome

KisSplice2RefGenome re-writing.
Code cleaning, update and simplification.
Add bioinformatics packages for GTF/GFF3 and SAM/BAM/(CRAM?) files reading.

# Organisation
We will mimic the organisation of the latest version (1.2.3), unless obvious improvements could be made.

- We keep the same set of parameters using argparse
    -> --counts default value is set to 2 to match KisSplice new default value
    -> --pairedEnd option is updated (store_true instead of store)
    -> --exonicReads option is updated (store_true instead of store)
    -> -chr option must be clarified