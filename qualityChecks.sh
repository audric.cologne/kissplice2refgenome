#!/bin/bash

k2rg="python ./kissplice2refgenome.py"
fMap="data/alignement.sam"
fMap_noE="data/alignement"
rl="--readLength 100"

# Error : non-existing files
$k2rg $rl thisfiledoesnotexist
$k2rg $rl $fMap -a thisfiledoesnotexist 
# Error : missing parameters
$k2rg $fMap # no readLength

# Error : incoherent parametrs
$k2rg $fMap $rl --del_IR 0
$k2rg $fMap $rl --readLength 0
$k2rg $fMap $rl --min-overlap 0
$k2rg $fMap $rl --noExonicReads --counts 0
$k2rg $fMap $rl --noExonicReads --counts 1
$k2rg $fMap $rl --order 1,2 3,4
$k2rg $fMap $rl --pairedEnd --order i,2 3,4
$k2rg $fMap $rl --pairedEnd --order 1,2 3,4,5
$k2rg $fMap $rl --pairedEnd --order 1,2 3,2
